﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#define LOG_TAG "coh_shimmer"
#include <coh_hisi/hi6250_shims.h>
#include <coh_hisi/shimmer.h>
#include <coh/list.h>
#include <coh/log.h>
#include <string.h>

static void create_list_of_shims(list_t *list, const char *path) {
	char *ld_shims = strdup(LD_SHIM_LIBS);
	char *entry = strtok(ld_shims, ":");
	while (entry) {
		if(!strncmp(entry,path, strlen(path)))
			list_add_entry(list, strdup(entry));
		entry = strtok(NULL, ":");
	}
}

void coh_get_shims_for_path(list_t *list, const char *path) {
	char *value;
	list_t shimlers_list = list_create("shims", 0, 1);
	create_list_of_shims(&shimlers_list, path);
	for(int i = 0; i < shimlers_list.count; i ++) {
		strtok((char *) list_get_at_index(&shimlers_list, i), "|");
		value = strtok(NULL, "|");
		if(!value)
			continue;
		LOGD("Found '%s' shim for %s",value,path);
		list_add_entry(list, strdup(value));
	}	
}