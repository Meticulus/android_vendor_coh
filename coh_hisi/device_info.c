﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#include <coh/file.h>
#include <coh_hisi/device_info.h>
#include <string.h>
#include <stdio.h>

int coh_get_model(char *buff, int buffsize) {
	return coh_read_from_file(PRODUCT_PATH, buff, buffsize);
}

int coh_get_codename(char *buff, int buffsize) {
	char ibuff[255] = {0};
	int ret = 1;
	if(coh_get_model(ibuff, 255)) {
		if(!strncmp(ibuff, "WAS", 3)) {
			strcpy(buff, "warsaw");
		} else if(!strncmp(ibuff, "PRA", 3)) {
			strcpy(buff, "prague");
		} else if(!strncmp(ibuff, "VNS", 3)) {
			strcpy(buff, "venus");
		} else if(!strncmp(ibuff, "NEM", 3) || !strncmp(ibuff, "NMO", 3)) {
			strcpy(buff, "nemo");
		} else if(!strncmp(ibuff, "BLN", 3) || !strncmp(ibuff, "BLL", 3)) {
			strcpy(buff, "nemo");
		} else if(!strncmp(ibuff, "BLK", 3)) {
			strcpy(buff, "berkeley");
		} else {
			strcpy(buff, "unknown");
			ret = 0;
		}
	} else
		ret = 0;
	return ret;
}

int coh_model_has_fbe() {
	char buff[255];
	coh_get_codename(buff, 255);
	if(strstr(FBE_DEVICES_MASK, buff))
		return 1;
	else
		return 0;
}

int coh_get_modemid(char * buff, int buffsize) {
	return coh_read_from_file(MODEM_ID_PATH, buff, buffsize);
}

void get_device_info(char *buff, int buffsize) {
	char thisbuff[255] = {0};
	char modemid[255] = {0};
	char *model;
	char *codename;
	coh_get_modemid(thisbuff, 255);
	sprintf(modemid, "0X%X%X%X%X%X", thisbuff[0], thisbuff[1],thisbuff[2],thisbuff[3],thisbuff[4]);
	coh_get_model(thisbuff, 255);
	model = strdup(thisbuff);
	coh_get_codename(thisbuff, 255);
	codename = strdup(thisbuff);
	int printed = sprintf(buff, "model: %s\n" \
				  "codename: %s\n" \
				  "modemid: %s\n",
				  model,
				  codename,
				  modemid );
}