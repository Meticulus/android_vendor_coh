﻿// /*
//  * Code Of Honor
//  * Copyright (c) 2018 Jonathan Dennis [Meticulus]
//  *                               theonejohnnyd@gmail.com
//  *
//  * This program is free software; you can redistribute it and/or modify
//  * it under the terms of the GNU General Public License as published by
//  * the Free Software Foundation; either version 2 of the License, or
//  * (at your option) any later version.
//  *
//  * This program is distributed in the hope that it will be useful,
//  * but WITHOUT ANY WARRANTY; without even the implied warranty of
//  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  * GNU General Public License for more details.
//  *
//  * You should have received a copy of the GNU General Public License
//  * along with this program; if not, write to the Free Software
//  * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//  */

#ifndef __DEVICE_INFO_H__
#define __DEVICE_INFO_H__

#define HISI_BASE "/sys/firmware/devicetree/base/"
#define PRODUCT_PATH HISI_BASE"hisi,product_name"
#define MODEM_ID_PATH HISI_BASE"hisi,modem_id"

#define FBE_DEVICES_MASK "warsaw prague"

int coh_get_model(char *buff, int buffsize);

int coh_get_codename(char *buff, int buffsize);

int coh_model_has_fbe();

int coh_get_modemid(char *buff, int buffsize);

void get_device_info(char *buff, int buffsize);

#endif

