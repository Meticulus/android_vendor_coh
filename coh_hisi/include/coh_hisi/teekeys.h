﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#ifndef __TEEKEYS_H__
#define __TEEKEYS_H__

typedef struct {
    char *name;
    int nameoffset;
    char *date;
    int dateoffset;
    char key[32];
    int keyoffset;
} teekey_t;

typedef struct {
   char buff[10000];
   int readsize;
   teekey_t fingerprintd;
   teekey_t keystore;
   teekey_t gatekeeperd;
} teekeys_t;

teekeys_t *readteekeys(char *path);
void updateteekeys(teekeys_t *keys);
void freeteekeys(teekeys_t *keys);


#endif