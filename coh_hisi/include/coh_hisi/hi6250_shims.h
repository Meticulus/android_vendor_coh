
/*
 * Copyright (C) 2017 Jonathan Dennis [Meticulus]
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _HI6250_SHIMS_H
#define _HI6250_SHIMS_H

#define COLON ":"

#define BERLIN_SHIMS "/hwvendor/lib64/hwcam/hwcam.hi6250.m.BERLIN.so|libshim_libgui.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.BERLIN.so|libshim_libui.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.BERLIN.so|libshim.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.BERLIN.so|libsensor.so"

#define DALLAS_SHIMS "/hwvendor/lib64/hwcam/hwcam.hi6250.m.DALLAS.so|libshim_libgui.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.DALLAS.so|libshim_libui.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.DALLAS.so|libshim.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.DALLAS.so|libsensor.so"

#define NEMO_SHIMS "/hwvendor/lib64/hwcam/hwcam.hi6250.m.NEMO.so|libshim_libgui.so" \
				   ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.NEMO.so|libshim_libui.so" \
				   ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.NEMO.so|libshim.so"

#define PRAGUE_SHIMS "/hwvendor/lib64/hwcam/hwcam.hi6250.m.PRAGUE.so|libshim_libgui.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.PRAGUE.so|libshim_libui.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.PRAGUE.so|libshim.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.PRAGUE.so|libsensor.so"

#define VENUS_SHIMS "/hwvendor/lib64/hwcam/hwcam.hi6250.m.VENUS.so|libshim_libgui.so" \
					":/hwvendor/lib64/hwcam/hwcam.hi6250.m.VENUS.so|libshim_libui.so" \
					":/hwvendor/lib64/hwcam/hwcam.hi6250.m.VENUS.so|libshim.so"

#define WARSAW_SHIMS "/hwvendor/lib64/hwcam/hwcam.hi6250.m.WARSAW.so|libshim_libui.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.WARSAW.so|libshim.so" \
					 ":/hwvendor/lib64/hwcam/hwcam.hi6250.m.WARSAW.so|libsensor.so"

#define COMMON_SHIMS "/hwvendor/lib/hw/audio.primary.hisi.so|libshim.so" \
					 ":/hwvendor/lib64/libcamera_algo.so|libshim_libui.so" \
					 ":/hwvendor/lib64/hw/fingerprint.hi6250.so|libshim.so" \
					 ":/hwvendor/lib64/libbalong-ril.so|libshim_libril.so"

#define OREO_SHIMS "/hwvendor/lib/hw/audio.primary.hisi.so|libshim_icu.so" \
				   ":/hwvendor/lib64/libhwuibp.so|libshim.so" \
				   ":/hwvendor/lib64/libcamera_algo.so|libsensor.so" \
				   ":/hwvendor/lib64/libgnss_sensors_hisi.so|libsensor.so"

#define OREO_BT_SHIMS "/system/lib64/hw/bluetooth.default.so|libshim_libmedia.so" \
					  ":/system/lib64/hw/bluetooth.default.so|libshim_libprotobuf-cpp-full.so" \
					  ":/system/lib64/hw/bluetooth.default.so|libshim_libicuuc.so" \
					  ":/system/lib64/hw/bluetooth.default.so|libshim_libicui18n.so"

#define LD_SHIM_LIBS COMMON_SHIMS COLON BERLIN_SHIMS COLON DALLAS_SHIMS COLON NEMO_SHIMS COLON PRAGUE_SHIMS COLON VENUS_SHIMS COLON WARSAW_SHIMS COLON OREO_SHIMS COLON OREO_BT_SHIMS
#endif
