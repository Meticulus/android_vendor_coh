﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#define LOG_TAG "coh_teekeys"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <coh/log.h>
#include <coh_hisi/teekeys.h>
#include <coh/file.h>
#include <stdlib.h>

static void printkey(char * key) {
    int i;
    char buff[1024];
    sprintf(buff,"%s", "key = { ");
    for(i = 0; i < 32; i ++) {
	    sprintf(buff,"%s0x%02X, ", strdup(buff),key[i]);
    }
    sprintf(buff, "%s%s",strdup(buff)," }");
    LOGD("%s", buff);
}

static void getkeyoffset(teekey_t *key, char *buff, int size) {
   for(int i = key->nameoffset -1; i > 0; i --) {
	char tmp = buff[i];
        if(tmp != '\0') {
	        key->dateoffset = i -13;
	        key->date = strdup((char *)buff + key->dateoffset);
	        LOGD("Date: %s\n", key->date);
	        key->keyoffset = key->dateoffset - 32;
	        memcpy(key->key, buff + key->keyoffset, 32);
	        printkey(key->key);
	        return;
	}
   }
   key->dateoffset = -1;
   key->keyoffset = -1;
   key->date = "";
   LOGE("%s", "Could not find dateoffset!");
}

static void getoffsets(teekey_t * key, char * buff, int size) {
    
   for(int i = 0; i < size; i++) {
	char * ctmp = (char *) buff + i;
	if(strlen(ctmp) > 0) {
	    if(!strcmp(ctmp, key->name)) {
		    key->nameoffset = i;
		    LOGD("found '%s' at offset %d\n", ctmp, i);
		    getkeyoffset(key, buff, size);
		    return;
	    }
	}
   }
   key->nameoffset = -1;
   key->dateoffset = -1;
   key->keyoffset = -1;
   key->date = "";
   LOGE("Could not find %s!", key->name);
}

teekeys_t *readteekeys(char *path) {
   int size = 0;
   teekeys_t *keys = (teekeys_t *) malloc(sizeof(teekeys_t));
   keys->readsize = coh_read_from_file(path, keys->buff, 10000);
   if(keys->readsize) {
	   keys->fingerprintd.name = "fingerprintd";
	   keys->keystore.name = "keystore";
	   keys->gatekeeperd.name = "gatekeeperd";
	   getoffsets(&keys->fingerprintd, keys->buff, keys->readsize);
	   getoffsets(&keys->keystore, keys->buff, keys->readsize);
	   getoffsets(&keys->gatekeeperd, keys->buff, keys->readsize);
   }
   return keys;
}

void updateteekeys(teekeys_t *keys) {
	memcpy(keys->buff + keys->fingerprintd.keyoffset, keys->fingerprintd.key, 32);
	memcpy(keys->buff + keys->keystore.keyoffset, keys->keystore.key, 32);
	memcpy(keys->buff + keys->gatekeeperd.keyoffset, keys->gatekeeperd.key, 32);
}

void freeteekeys(teekeys_t *keys) {
	free(keys);
}