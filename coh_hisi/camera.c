﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#define LOG_TAG "coh_hisi_camera"
#include <coh_hisi/camera.h>
#include <coh_hisi/device_info.h>
#include <coh/list.h>
#include <coh/log.h>
#include <string.h>

static void create_params_list(list_t *p, const char* params) {
	char *pcopy = strdup(params);
	char *item = strtok(pcopy, ";");
	while(item) {
		list_add_entry(p, strdup(item));
		item = strtok(NULL, ";");
	}
}

static void log_params(list_t *params) {
	for(int i = 0; i < params->count; i ++){
		char * entry = (char *) list_get_at_index(params, i);
		LOGI("%s", entry);
	}
}

static char *filter_comma_error(char * kvp) {
	char *key = strtok(kvp, "=");
	char *value = strtok(NULL, "=");
	static char *ret = NULL;
	LOGD("key=%s value=%s", key, value);

	if(!strncmp(value, ",", 1)) {
#ifdef __ANDROID__
		char model[255];
		coh_get_model(model, 255);
		LOGE("Found comma bug in %s : %s", key, model);
#else
	LOGE("Found comma bug in %s", key);
#endif
		LOGE("Correcting comma bug in %s", key);
		value = value + 1;
	}
	asprintf(&ret, "%s=%s", key,value);
	return ret;
}

char * coh_prep_cam_parameters(const char* params) {
	LOGI("%s", params);
	char *ret = "";
	char *item = NULL;
	list_t plist = list_create("cam params", 10, 10);
	create_params_list(&plist, params);
	log_params(&plist);
	item = filter_comma_error((char *) list_get_at_index(&plist, 0));
	asprintf(&ret, "%s", item);
	for(int i = 1; i < plist.count; i ++) {
		item = filter_comma_error((char *) list_get_at_index(&plist, i));
		asprintf(&ret, "%s;%s", ret, item);
	}
	LOGI("%s",ret);
	return strdup(ret);
}