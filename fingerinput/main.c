/*
 *   Copyright (C) 2017  Jonathan Jason Dennis [Meticulus]
 *                                       theonejohnnyd@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.#include <linux/input.h>
 */

#include <linux/input.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <coh/file.h>

#ifndef FINGERINPUTPATH
#define FINGERINPUTPATH "/dev/input/event3"
#endif

int main(int argc, char *argv[]) {
    int fd = -1,retval = -2;
    struct input_event ev ;
    int size = sizeof(struct input_event);
restart:
	coh_read_from_file(FINGERINPUTPATH, &ev, size);
	printf("ev.type = %d\n", ev.type);
	if(ev.type ==  1) {
	    switch (ev.code) {
		case 28:
		    printf("enter\n");
		    retval = -1;
		    break;
		case 111:
		    printf("doublclick\n");
		    retval = 0;
		    break;
		default:
			printf("value = %d\n", ev.code);
		    goto restart;
	    }
	close(fd);
    } else {
		printf("Could not open %s\n", FINGERINPUTPATH);
    }
    return retval;
}


