﻿#include <stdio.h>
#include <coh/file.h>
#include <coh_linux/cpu.h>
#include <coh/list.h>
#include <coh_android/prop_parser.h>
#include <string.h>
#include <coh_hisi/camera.h>
#include <coh_hisi/shimmer.h>
#define LOG_TAG "coh_test"
#define DEBUG
#include <coh/log.h>
#include <coh_linux/gpu.h>
#include <coh_hisi/teekeys.h>

static void read_test() {
 char buff[255] = {0};
	if(coh_read_from_file("/home/meticulus/Desktop/cpu.txt", buff, 255))
		printf("read %s\n", buff);
}

static void prop_incomming(char *key, char *value) {
	printf("key:%s value:%s\n", key,value);
}

static void prop_parser_test() {
	coh_parse_prop_file("/system/build.prop", &prop_incomming);
}
static void write_test() {
	char *buff = "test";
	if(coh_write_to_file("/tmp/test.txt", buff, strlen(buff)))
		coh_read_from_file("/tmp/test.txt", buff, strlen(buff));
}

static void camera_params_parse_test() {
	coh_prep_cam_parameters("test=true;test1=,false;test2=true");
}

static void shim_test() {
	list_t list = list_create(NULL,0,1);
	coh_get_shims_for_path(&list, "/hwvendor/lib64/hwcam/hwcam.hi6250.m.NEMO.so");
}

static void cpu_test() {
	coh_cpu_cpufreq_t *cpufreq = get_cpufreq(0);
	dump_cpufreq(cpufreq);
	cpufreq = get_cpufreq(4);
	dump_cpufreq(cpufreq);
}

static void printkey(char * key) {
    int i;
    char buff[1024];
    sprintf(buff,"%s", "key = { ");
    for(i = 0; i < 32; i ++) {
	    sprintf(buff,"%s0x%02X, ", strdup(buff),key[i]);
    }
    sprintf(buff, "%s%s",strdup(buff)," }");
    LOGD("%s", buff);
}

int main (int argc, char *argv[])
{
	char dummykey[32] = { 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xAA };
	char readkey[32];
	/*teekeys_t *keys = readteekeys("/home/meticulus/Desktop/native_packages.xml");
	memcpy(keys->fingerprintd.key, dummykey, 32);
	memcpy(keys->keystore.key, dummykey, 32);
	memcpy(keys->gatekeeperd.key, dummykey, 32);
	updateteekeys(keys);
	coh_write_to_file("/home/meticulus/Desktop/newnative_packages.xml", keys->buff, keys->readsize);
	*/
	//coh_write_to_file("/sys/module/tc_client_driver/parameters/fpkey", dummykey, 32);
	coh_read_from_file("/sys/module/tc_client_driver/parameters/fpkey", readkey, 32);
	printkey(readkey);
}

