/*
 *   Copyright (C) 2017  Jonathan Jason Dennis [Meticulus]
 *                                       theonejohnnyd@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.#include <linux/input.h>
 */

#include <linux/input.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#ifdef __ANDROID__
#include <cutils/klog.h>
#endif
#include <coh/file.h>

#define USB_SWITCH_PATH "/sys/class/usb_switch/switch_ctrl/manual_ctrl/switchctrl"
#define PLUGUSB_PATH "/sys/devices/platform/ff100000.hisi_usb/plugusb"
#define CMDLINE_PATH "/proc/cmdline"
#define BLUE_PATH "/sys/class/leds/blue/brightness"

int check_recovery() {
    int fd = -1, ret = 0;
    char buff[255];
    if(coh_read_from_file(CMDLINE_PATH, buff, 254))
    	if(strstr(buff, "enter_recovery=1"))
			ret = 1;

    return ret;
}

int main(int argc, char *argv[]) {
    int fd = -1,retval = -2,switchval = 0, hoston = 0, in_recovery = 0;
    char buff[255];
#ifdef __ANDROID__
    klog_write(0, "coh_otgd: Startin' up.");
#endif
    in_recovery = check_recovery();
    fd = open(USB_SWITCH_PATH, O_RDONLY);
    if(!fd) {
#ifdef __ANDROID__
	klog_write(0, "coh_otgd: Could not open %s; exiting!\n", USB_SWITCH_PATH);
#endif
	exit(-1);
    }
    while(1) {
	pread(fd, buff, 254, 0);
	switchval = atoi(buff);
	if(!hoston && switchval == 8) {
#ifdef __ANDROID__
	    klog_write(0, "coh_otgd: OTG Cable plug detected. hoston!");
#endif
	    coh_write_to_file(PLUGUSB_PATH, "hoston", 6);
	    hoston = 1;
	    if(in_recovery)
			coh_write_to_file(BLUE_PATH, "255", 3);
	} else if (hoston && switchval != 8) {
#ifdef __ANDROID__
	    klog_write(0, "coh_otgd: OTG Cable unplug detected. hostoff!");
#endif
	    coh_write_to_file(PLUGUSB_PATH, "hostoff", 7);
	    hoston = 0;
	    if(in_recovery)
		coh_write_to_file(BLUE_PATH, "0", 1);
	}
	
	sleep(1);
    }
}


