# Copyright 2018 Jonathan Jason Dennis [Meticulus]
#			theonejohnnyd@gmail.com
#
# Android.mk for CoH OTG Daemon
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../coh_common/include
LOCAL_STATIC_LIBRARIES := libcoh_common liblog libcutils
LOCAL_SRC_FILES := main.c
LOCAL_MODULE := coh_rotgd
LOCAL_FORCE_STATIC_EXECUTABLE := true
LOCAL_MODULE_PATH := $(TARGET_RECOVERY_ROOT_OUT)/sbin
LOCAL_UNSTRIPPED_PATH := $(TARGET_ROOT_OUT_SBIN_UNSTRIPPED)
LOCAL_POST_INSTALL_CMD := cp $(LOCAL_PATH)/init.recovery.coh.otgd.rc $(TARGET_RECOVERY_ROOT_OUT)
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../coh_common/include
LOCAL_SHARED_LIBRARIES := libcoh_common liblog libcutils
LOCAL_SRC_FILES := main.c
LOCAL_MODULE := coh_otgd
LOCAL_INIT_RC := coh_otgd.rc
include $(BUILD_EXECUTABLE)

