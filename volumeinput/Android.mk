LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../coh_common/include
LOCAL_MODULE := volumeinput
LOCAL_SRC_FILES := \
    main.c
LOCAL_STATIC_LIBRARIES := libcoh_common liblog libc
ifneq ($(COH_VOLUME_EVENT),)
LOCAL_CFLAGS += -DVOLUMEINPUTPATH=\"$(COH_VOLUME_EVENT)\"
endif
LOCAL_FORCE_STATIC_EXECUTABLE := true
include $(BUILD_EXECUTABLE)

