﻿/*
 * Code Of Honor
 * Copyright (c) 2018 Jonathan Dennis [Meticulus]
 *                               theonejohnnyd@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define LOG_TAG "coh_linux_cpu"

#include <coh_linux/cpu.h>
#include <coh/file.h>
#include <stdio.h>
#include <string.h>
#include <coh/log.h>
#include <stdlib.h>
#include <unistd.h>

#define CPU_PATH_PREFIX "/sys/devices/system/cpu/cpu"
#define CPU_FREQ_SUFFIX "/cpufreq"

static char *create_cpufreq_path(char *basename, int core) {
	char path[255] = {0};
	sprintf(path, "%s%d%s/%s",CPU_PATH_PREFIX,core,CPU_FREQ_SUFFIX, basename);
	return strdup(path); 
}

static char *create_cpufreq_gov_path(char *basename, int core, char *gov) {
	char path[255] = {0};
	sprintf(path, "%s%d%s/%s/%s",CPU_PATH_PREFIX,core,CPU_FREQ_SUFFIX, gov, basename);
	return strdup(path); 
}

static char *read_file(char *path) {
	char buff[255] = {0};
	if(coh_read_from_file(path, buff, 255)) {
		char *nl = strstr(buff, "\n");
		if(nl) nl[0] = '\0';
	}
	return strdup(buff);
}

static void dump_list(list_t *list) {
	for(int i=0; i < list->count; i ++) {
		LOGD("    '%s'", (char *) list_get_at_index(list, i));
	}
} 

void dump_cpufreq(coh_cpu_cpufreq_t *cpufreq) {
	LOGD("cpufreq->core = '%d'", cpufreq->core);
	LOGD("%s", "cpufreq->affected_cpus:");
	dump_list(&cpufreq->affected_cpus);
	LOGD("cpufreq->msg_policy = '%s'", cpufreq->msg_policy);
	LOGD("%s","cpufreq->related_cpus:");
	dump_list(&cpufreq->related_cpus);
	LOGD("cpufreq->cpuinfo.cpuinfo_cur_freq = '%d'", cpufreq->cpuinfo.cpuinfo_cur_freq);
	LOGD("cpufreq->cpuinfo.cpuinfo_max_freq = '%d'", cpufreq->cpuinfo.cpuinfo_max_freq);
	LOGD("cpufreq->cpuinfo.cpuinfo_min_freq = '%d'", cpufreq->cpuinfo.cpuinfo_min_freq);
	LOGD("cpufreq->cpuinfo.cpuinfo_transition_latency = '%d'", cpufreq->cpuinfo.cpuinfo_transition_latency);
	LOGD("%s","cpufreq->scaling.scaling_available_frequencies:");
	dump_list(&cpufreq->scaling.scaling_available_frequencies);
	LOGD("%s","cpufreq->scaling.scaling_available_governors:");
	dump_list(&cpufreq->scaling.scaling_available_governors);
	LOGD("cpufreq->scaling.scaling_cur_freq = '%d'", cpufreq->scaling.scaling_cur_freq);
	LOGD("cpufreq->scaling.scaling_driver = '%s'", cpufreq->scaling.scaling_driver);
	LOGD("cpufreq->scaling.scaling_governor.name = '%s'", cpufreq->scaling.scaling_governor.name);
	LOGD("cpufreq->scaling.scaling_governor.has_boost = '%d'", cpufreq->scaling.scaling_governor.has_boost);
	LOGD("cpufreq->scaling.scaling_max_freq = '%d'", cpufreq->scaling.scaling_max_freq);
	LOGD("cpufreq->scaling.scaling_min_freq = '%d'", cpufreq->scaling.scaling_min_freq);
	LOGD("cpufreq->scaling.scaling_stepspeed = '%d'", cpufreq->scaling.scaling_setspeed);
	LOGD("%s","cpufreq->stats.time_in_state:");
	dump_list(&cpufreq->stats.time_in_state);
	LOGD("cpufreq->stats.total_trans = '%d'", cpufreq->stats.total_trans);
	LOGD("%s", "cpufreq->stats.trans_table:");
	LOGD("%s", cpufreq->stats.trans_table);
}
coh_cpu_cpufreq_t *get_cpufreq(int core) {
	coh_cpu_cpufreq_t *cpufreq = malloc(sizeof(coh_cpu_cpufreq_t));
	cpufreq->core = core;
	cpufreq->affected_cpus = read_file_and_split(create_cpufreq_path("affected_cpus", core), " ", 1024);
	cpufreq->msg_policy = read_file(create_cpufreq_path("msg_policy", core));
	cpufreq->related_cpus = read_file_and_split(create_cpufreq_path("related_cpus", core), " ", 1024);

	cpufreq->cpuinfo.cpuinfo_cur_freq = atoi(read_file(create_cpufreq_path("cpuinfo_cur_freq", core)));
	cpufreq->cpuinfo.cpuinfo_max_freq = atoi(read_file(create_cpufreq_path("cpuinfo_max_freq", core)));
	cpufreq->cpuinfo.cpuinfo_min_freq = atoi(read_file(create_cpufreq_path("cpuinfo_min_freq", core)));
	cpufreq->cpuinfo.cpuinfo_transition_latency = 
		atoi(read_file(create_cpufreq_path("cpuinfo_transition_latency", core)));

	cpufreq->scaling.scaling_available_frequencies = 
		read_file_and_split(create_cpufreq_path("scaling_available_frequencies", core), " ", 1024);
	cpufreq->scaling.scaling_available_governors = 
		read_file_and_split(create_cpufreq_path("scaling_available_governors", core), " ", 1024);
	cpufreq->scaling.scaling_cur_freq = atoi(read_file(create_cpufreq_path("scaling_cur_freq", core)));
	cpufreq->scaling.scaling_driver = read_file(create_cpufreq_path("scaling_driver", core));
	cpufreq->scaling.scaling_governor.name = read_file(create_cpufreq_path("scaling_governor", core));
	cpufreq->scaling.scaling_governor.has_boost = 
		!access(create_cpufreq_gov_path("boost",core, cpufreq->scaling.scaling_governor.name), W_OK);
	cpufreq->scaling.scaling_max_freq = atoi(read_file(create_cpufreq_path("scaling_max_freq", core)));
	cpufreq->scaling.scaling_min_freq = atoi(read_file(create_cpufreq_path("scaling_min_freq", core)));
	cpufreq->scaling.scaling_setspeed = atoi(read_file(create_cpufreq_path("scaling_setspeed", core)));

	cpufreq->stats.time_in_state = read_file_and_split(create_cpufreq_path("stats/time_in_state", core), "\n", 1024);
	cpufreq->stats.total_trans = atoi(read_file(create_cpufreq_path("stats/total_trans", core)));
	cpufreq->stats.trans_table = read_file(create_cpufreq_path("stats/trans_table", core));
	return cpufreq;

}

void get_cpufreq_info(int core, char* buff, int size) {
	coh_cpu_cpufreq_t *cpufreq = get_cpufreq(core); 
	int printed = sprintf(buff, "core: %d\n" \
				  "cpuinfo_max_freq: %d\n" \
				  "cpuinfo_min_freq: %d\n" \
				  "cpuinfo_transition_latency: %d\n" \
				  "scaling_available_frequencies: %s\n" \
				  "scaling_available_governors: %s\n" \
				  "scaling_driver: %s\n" \
				  "scaling_governor: %s\n" \
				  "scaling_governor_has_boost: %d\n" \
				  "scaling_max_freq: %d\n" \
				  "scaling_min_freq: %d\n" \
				  "scaling_setspeed: %d\n", 
				  cpufreq->core,
				  cpufreq->cpuinfo.cpuinfo_max_freq,
				  cpufreq->cpuinfo.cpuinfo_min_freq,
				  cpufreq->cpuinfo.cpuinfo_transition_latency,
				  cpufreq->scaling.scaling_available_frequencies,
				  cpufreq->scaling.scaling_available_governors,
				  cpufreq->scaling.scaling_driver,
				  cpufreq->scaling.scaling_governor.name,
				  cpufreq->scaling.scaling_governor.has_boost,
				  cpufreq->scaling.scaling_max_freq,
				  cpufreq->scaling.scaling_min_freq,
				  cpufreq->scaling.scaling_setspeed );
}