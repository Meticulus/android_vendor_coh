﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#ifndef __COH_GPU_H__
#define __COH_GPU_H__

#include <coh/list.h>

typedef struct {
	char *name;
	int has_animboost;
} coh_gpu_gov_t;

typedef struct {
	list_t available_frequencies;
	list_t available_governors;
	int cur_freq;
	coh_gpu_gov_t governor;
	int max_freq;
	int min_freq;
	int polling_interval;
	int target_freq;
} coh_gpu_devfreq_t;

void dump_gpu_devfreq(coh_gpu_devfreq_t *gpu);
coh_gpu_devfreq_t *get_gpu_devfreq();
void get_gpu_devfreq_info(char* buff, int size);

#endif