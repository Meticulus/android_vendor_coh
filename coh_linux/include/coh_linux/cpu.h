﻿
#ifndef __CPU_H__
#define __CPU_H__

#include <coh/list.h>

typedef struct {
	int cpuinfo_cur_freq;
	int cpuinfo_max_freq;
	int cpuinfo_min_freq;
	int cpuinfo_transition_latency;
} coh_cpu_cpuinfo_t;

typedef struct {
	char *name;
	int has_boost;
} coh_cpu_governor_t;

typedef struct {
	list_t scaling_available_frequencies;
	list_t scaling_available_governors;
	int scaling_cur_freq;
	char *scaling_driver;
	coh_cpu_governor_t scaling_governor;
	int scaling_max_freq;
	int scaling_min_freq;
	int scaling_setspeed;
} coh_cpu_scaling_t;

typedef struct {
	list_t time_in_state;
	int total_trans;
	char *trans_table;
} coh_cpu_stats_t;

typedef struct {
	int core;
	list_t affected_cpus;
	char *msg_policy;
	list_t related_cpus;
	coh_cpu_cpuinfo_t cpuinfo;
	coh_cpu_scaling_t scaling;
	coh_cpu_stats_t stats;
} coh_cpu_cpufreq_t;

void dump_cpufreq(coh_cpu_cpufreq_t *cpufreq);
coh_cpu_cpufreq_t *get_cpufreq(int core);
void get_cpufreq_info(int core, char *buff, int size);

#endif

