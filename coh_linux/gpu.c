﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#define LOG_TAG "coh_linux_gpu"
#define GPU_FREQ_PATH "/sys/class/devfreq/gpufreq"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <coh/log.h>
#include <coh_linux/gpu.h>
#include <coh/file.h>
#include <unistd.h>

static char* create_gpu_gov_path(char *basename, char* gov) {
	char buff[255] = {0};
	sprintf(buff, "%s/%s/%s", GPU_FREQ_PATH,gov,basename);
	return strdup(buff);
}
static char* create_gpu_path(char *basename) {
	char buff[255] = {0};
	sprintf(buff, "%s/%s", GPU_FREQ_PATH, basename);
	return strdup(buff);
}

static char *read_file(char *path) {
	char buff[255] = {0};
	if(coh_read_from_file(path, buff, 255)) {
		char *nl = strstr(buff, "\n");
		if(nl) nl[0] = '\0';
	}
	return strdup(buff);
}

static void dump_list(list_t *list) {
	for(int i=0; i < list->count; i ++) {
		LOGD("    '%s'", (char *) list_get_at_index(list, i));
	}
} 

void dump_gpu_devfreq(coh_gpu_devfreq_t *gpu) {
	LOGD("%s","gpu->available_frequencies:");
	dump_list(&gpu->available_frequencies);
	LOGD("%s","gpu->available_governors:");
	dump_list(&gpu->available_governors);
	LOGD("gpu->cur_freq = '%d'", gpu->cur_freq);
	LOGD("gpu->governor.name = '%s'", gpu->governor.name);
	LOGD("gpu->governor.has_animboost = '%d'", gpu->governor.has_animboost);
	LOGD("gpu->max_freq = '%d'", gpu->max_freq);
	LOGD("gpu->min_freq = '%d'", gpu->min_freq);
	LOGD("gpu->polling_interval = '%d'", gpu->polling_interval);
	LOGD("gpu->target_freq = '%d'", gpu->target_freq);
}

coh_gpu_devfreq_t *get_gpu_devfreq() {
	coh_gpu_devfreq_t *gpu = malloc(sizeof(coh_gpu_devfreq_t));
	gpu->available_frequencies = read_file_and_split(create_gpu_path("available_frequencies"), " ", 1024);
	gpu->available_governors = read_file_and_split(create_gpu_path("available_governors"), " ", 1024);
	gpu->cur_freq = atoi(read_file(create_gpu_path("cur_freq")));
	gpu->governor.name = read_file(create_gpu_path("governor"));
	gpu->governor.has_animboost = !access(create_gpu_gov_path("animation_boost", gpu->governor.name), W_OK);
	gpu->max_freq = atoi(read_file(create_gpu_path("max_freq")));
	gpu->min_freq = atoi(read_file(create_gpu_path("min_freq")));
	gpu->polling_interval = atoi(read_file(create_gpu_path("polling_interval")));
	gpu->target_freq = atoi(read_file(create_gpu_path("target_freq")));
	return gpu;
}

void get_gpu_devfreq_info(char* buff, int size) {
	coh_gpu_devfreq_t *gpu = get_gpu_devfreq(); 
	int printed = sprintf(buff, "available_frequencies: %s\n" \
				  "available_governors: %s\n" \
				  "governor: %s\n" \
				  "governor_animation_boost: %d\n" \
				  "max_freq: %d\n" \
				  "min_freq: %d\n" \
				  "polling_interval: %d\n" \
				  "target_freq: %d\n",
				  gpu->available_frequencies,
				  gpu->available_governors,
				  gpu->governor.name,
				  gpu->governor.has_animboost,
				  gpu->max_freq,
				  gpu->min_freq,
				  gpu->polling_interval,
				  gpu->target_freq );
}