﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#define LOG_TAG "coh_torch"

#include <coh/log.h>
#include <coh/file.h>
#include <coh_linux/torch.h>
#include <unistd.h>

#define TORCH_FILE "/sys/class/leds/torch/brightness"

int coh_torch_is_supported() {
	return !access(TORCH_FILE, W_OK);
}

int coh_set_torch(int mode) {
	if(mode)
		return coh_write_to_file(TORCH_FILE, "255", 3);
	else
		return coh_write_to_file(TORCH_FILE, "0", 1);
}