﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#define LOG_TAG "coh_led"
#include <coh_linux/led.h>
#include <coh/log.h>
#include <coh/file.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define RED_BRIGHTNESS_FILE "/sys/class/leds/red/brightness"
#define RED_DELAYON_FILE "/sys/class/leds/red/delay_on"
#define RED_DELAYOFF_FILE "/sys/class/leds/red/delay_off"

#define GREEN_BRIGHTNESS_FILE "/sys/class/leds/green/brightness"
#define GREEN_DELAYON_FILE "/sys/class/leds/green/delay_on"
#define GREEN_DELAYOFF_FILE "/sys/class/leds/green/delay_off"

#define BLUE_BRIGHTNESS_FILE "/sys/class/leds/blue/brightness"
#define BLUE_DELAYON_FILE "/sys/class/leds/blue/delay_on"
#define BLUE_DELAYOFF_FILE "/sys/class/leds/blue/delay_off"

static pthread_t blinkt;

static int blinking = 0;

static void int_to_argb(int color, int argb[]) {
        char scolor[255];
        char alpha[3], red[3], green[3], blue[3];
        sprintf(scolor, "%x", color);

        LOGD("color=%s", scolor);

        strncpy(alpha, scolor, 2);
        strncpy(red, scolor + 2, 2);
        strncpy(green, scolor + 4, 2); 
        strncpy(blue, scolor + 6, 2);
        alpha[2] = red[2] = green[2] = blue[2] = '\0';

        LOGD("alpha=%s red=%s green=%s blue=%s", alpha, red, green, blue);

        argb[0] = (int)strtol(alpha, NULL, 16);
        argb[1] = (int)strtol(red, NULL, 16);
        argb[2] = (int)strtol(green, NULL, 16);
        argb[3] = (int)strtol(blue, NULL, 16);

}


static void set_led_color_internal(int color) {
	int argb[4];
	int_to_argb(color, argb);
	coh_write_int_to_file(RED_BRIGHTNESS_FILE, argb[1]);
	coh_write_int_to_file(GREEN_BRIGHTNESS_FILE, argb[2]);
	coh_write_int_to_file(BLUE_BRIGHTNESS_FILE, argb[3]);
}

static void *blink_thread_method(void *arg) {
	int color = * (int *)arg;
	while(blinking) {
		set_led_color_internal(color);
		sleep(1);
		set_led_color_internal(0x00000000);
		sleep(1);
	}
	return NULL;
}

void set_led_color(int color, int blink) {
	void *retval;
	if(blink) {
		blinking = 1;
		pthread_create(&blinkt, NULL, blink_thread_method, &color);
	} else {
		blinking = 0;
		if(blinkt) {
			pthread_join(blinkt, &retval);
		}
		set_led_color_internal(color);
	}
}

void set_led_color_on_batterylevel(int batterylevel) {
	if (batterylevel < 15) {
		set_led_color(0xFFFF0000, 0);
	} else if (batterylevel < 90) {
		set_led_color(0xFFFFFF00, 0);
	} else {
		set_led_color(0xFF00FF00, 0);
	}
}
