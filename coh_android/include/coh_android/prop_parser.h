﻿
#ifndef __PROP_PARSER_H__
#define __PROP_PARSER_H__

typedef void (* prop_parsed_cb)(char *key, char* value);

void coh_parse_prop_file(char *path, prop_parsed_cb cb);

#endif

