﻿/*
 * Code Of Honor
 * Copyright (c) 2018 Jonathan Dennis [Meticulus]
 *                               theonejohnnyd@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define LOG_TAG "coh_prop_parser"
#include <coh_android/prop_parser.h>
#include <coh/log.h>
#include <stdio.h>
#include <string.h>

void coh_parse_prop_file(char *path, prop_parsed_cb cb) {
	char *buff = NULL;
	char *key = NULL;
	char *value = NULL;
	size_t size = 0;
    FILE * fp = fopen(path,"r");
    if(!fp) {
		LOGE("Can't open %s", path);
		return;
    }
    while (getline(&buff, &size,fp) > 0) {
        buff[strlen(buff) -1] = '\0';
        //Skip comments
        if(!strncmp(buff, "#",1))
        	continue;
        //Skip empty lines
        if(!strstr(buff, "="))
        	continue;
     	key = strtok(buff, "=");
     	value = strtok(NULL, "=");
     	cb(key,value);
	}

}

