﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#define LOG_TAG "coh_zygote_whitelist"
#include <string.h>
#include <coh/log.h>
#include <coh_android/zygote_whitelist.h>

#define PERMISSIVE 1
#define WHITELIST "/sys/firmware/devicetree/base/hisi,product_name" \
				  "/sys/firmware/devicetree/base/hisi,modem_id" \
				  "/hwvendor/phone.prop" \
				  "/vendor/phone.prop" \
				  "/dev/__properties__"

int coh_is_zygote_allowed_path(const char *path) {
	if(strstr(WHITELIST, path))
		return 1;
	else if (!strncmp(path, "/product", strlen("/product")))
		return 1;
	else if(PERMISSIVE){
		LOGE("%s is not in the zygote whitelist but permissive is set.", path);
		return 1;
	} else
		return 0;
		

}