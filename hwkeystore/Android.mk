# Copyright (C) 2011 The Android Open Source Project
# Copyright (C) 2018 Meticulus && Code Of Honor
#				theonejohnnyd@gmail.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

KEYSTORE_PATH := ../../../system/security/keystore
KEYMASTER_DEFAULTSVC_PATH := ../../../hardware/interfaces/keymaster/3.0/default

ifneq ($(TARGET_BUILD_PDK),true)
include $(CLEAR_VARS)
ifeq ($(USE_32_BIT_KEYSTORE), true)
LOCAL_MULTILIB := 32
endif
LOCAL_CFLAGS := -Wall -Wextra -Wunused
LOCAL_SRC_FILES := \
        $(KEYSTORE_PATH)/auth_token_table.cpp \
        $(KEYSTORE_PATH)/blob.cpp \
        $(KEYSTORE_PATH)/entropy.cpp \
        $(KEYSTORE_PATH)/key_store_service.cpp \
        $(KEYSTORE_PATH)/keystore_attestation_id.cpp \
        $(KEYSTORE_PATH)/keyblob_utils.cpp \
        $(KEYSTORE_PATH)/keystore.cpp \
        keystore_main.cpp \
        $(KEYSTORE_PATH)/keystore_utils.cpp \
        $(KEYSTORE_PATH)/legacy_keymaster_device_wrapper.cpp \
        $(KEYSTORE_PATH)/keymaster_enforcement.cpp \
        $(KEYSTORE_PATH)/operation.cpp \
        $(KEYSTORE_PATH)/permissions.cpp \
        $(KEYSTORE_PATH)/user_state.cpp \
        $(KEYSTORE_PATH)/grant_store.cpp \
        ../../../frameworks/base/core/java/android/security/keymaster/IKeyAttestationApplicationIdProvider.aidl \
	service.cpp
LOCAL_SHARED_LIBRARIES := \
        libbinder \
        libcutils \
        libcrypto \
        libhardware \
        libwifikeystorehal \
        libkeystore_binder \
        liblog \
        libsoftkeymaster \
        libutils \
        libselinux \
        libsoftkeymasterdevice \
        libkeymaster_messages \
        libkeymaster_portable \
        libkeymaster_staging \
        libhwbinder \
        libhidlbase \
        libhidltransport \
        android.hardware.keymaster@3.0 \
	android.system.wifi.keystore@1.0 \
    	libdl \
    	libbase \
    	libhardware \
    	libhidlbase \
    	libhidltransport


LOCAL_HEADER_LIBRARIES := libbase_headers
LOCAL_OVERRIDES_MODULES := keystore
LOCAL_MODULE := hwkeystore
LOCAL_MODULE_TAGS := optional
LOCAL_INIT_RC := $(KEYSTORE_PATH)/keystore.rc
LOCAL_C_INCLUES := \
    system/keymaster/ \
    system/hardware/interfaces/wifi/keystore/1.0/default/include
LOCAL_CLANG := true
LOCAL_SANITIZE := integer
LOCAL_ADDITIONAL_DEPENDENCIES := $(KEYSTORE_PATH)/Android.mk
LOCAL_AIDL_INCLUDES := frameworks/base/core/java/

LOCAL_POST_INSTALL_CMD := mv $(TARGET_OUT)/bin/hwkeystore $(TARGET_OUT)/bin/keystore

include $(BUILD_EXECUTABLE)

endif
