#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

GATEKEEPERD_PATH := ../../../system/core/gatekeeperd
GATEKEEPERDHW_PATH := ../../../hardware/interfaces/gatekeeper/1.0/default

include $(CLEAR_VARS)
LOCAL_CFLAGS := -Wall -Wextra  -Wunused
LOCAL_SRC_FILES := \
	$(GATEKEEPERD_PATH)/SoftGateKeeperDevice.cpp \
	$(GATEKEEPERD_PATH)/IGateKeeperService.cpp \
	gatekeeperd.cpp \
	service.cpp 

LOCAL_OVERRIDES_MODULES := gatekeeperd
LOCAL_MODULE := hwgatekeeperd
LOCAL_SHARED_LIBRARIES := \
	libbinder \
	libgatekeeper \
	liblog \
	libhardware \
	libbase \
	libutils \
	libcrypto \
	libkeystore_binder \
	libhidlbase \
	libhidltransport \
	libhwbinder \
	android.hardware.gatekeeper@1.0

LOCAL_STATIC_LIBRARIES := libscrypt_static
LOCAL_C_INCLUDES := external/scrypt/lib/crypto
LOCAL_INIT_RC := $(GATEKEEPERD_PATH)/gatekeeperd.rc

LOCAL_POST_INSTALL_CMD := mv $(TARGET_OUT)/bin/hwgatekeeperd $(TARGET_OUT)/bin/gatekeeperd

include $(BUILD_EXECUTABLE)

