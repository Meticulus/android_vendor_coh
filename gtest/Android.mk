LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES += $(TOP)/bootable/recovery/minui/include
LOCAL_MODULE := gtest
LOCAL_SRC_FILES := \
    main.cpp
LOCAL_FORCE_STATIC_EXECUTABLE := true
LOCAL_STATIC_LIBRARIES := libminui libpng libz libc
include $(BUILD_EXECUTABLE)

