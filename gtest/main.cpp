﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#include <stdio.h>
#include <minui/minui.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
	printf ("Hello world!\n");
	gr_init();
	gr_fb_blank(false);
	gr_color(255,255,255,255);
	gr_fill(0,0, gr_fb_width(), gr_fb_height());
	gr_color(100,100,100,255);
	gr_text(gr_sys_font(),0,0, "test",true);
	gr_flip();
	while(true)
		sleep(10);
	gr_exit();
	
	return 0;
}

