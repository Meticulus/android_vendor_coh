﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#define LOG_TAG "coh_powerd"
#include <stdio.h>
#include <coh_linux/cpu.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <coh/log.h>
#include <unistd.h>
#include <fcntl.h>

int main (int argc, char *argv[])
{
	coh_cpu_cpufreq_t *cpufreq = get_cpufreq(0);
	dump_cpufreq(cpufreq);
	cpufreq = get_cpufreq(4);
	dump_cpufreq(cpufreq);

	mkfifo ("/tmp/test", S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	char buff[255] = {0};
	while (1) {
		int fd = open("/tmp/test", O_RDONLY);
		read(fd, buff, 255);
		LOGD("%s", buff);
		close(fd);
	}
}

