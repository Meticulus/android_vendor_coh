LOCAL_PATH := $(call my-dir)

BIOMETRICSHW_PATH := ../../../hardware/interfaces/biometrics/fingerprint/2.1/default

include $(CLEAR_VARS)
LOCAL_MODULE := fingerprintd
LOCAL_SRC_FILES := \
    BiometricsFingerprint.cpp \
    $(BIOMETRICSHW_PATH)/service.cpp \

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    liblog \
    libhidlbase \
    libhidltransport \
    libhardware \
    libutils \
    android.hardware.biometrics.fingerprint@2.1 \

include $(BUILD_EXECUTABLE)
