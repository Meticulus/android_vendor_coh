//
//  list.h
//
//  Author:
//       Jonathan Jason Dennis <theonejohnnyd@gmail.com>
//
//  Copyright (c) 2017 Meticulus Development
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef FILE_H
#define FILE_H

#include <coh/list.h>

typedef struct {
	char* filename;
	int fd;
} coh_filebase_t;

typedef struct {
	coh_filebase_t fb;
	int can_read;
	int can_write;
} coh_file_t;

int coh_read_from_file(char *path, void* buff, int buffsize);
int coh_write_to_file(char *path, void* buff, int buffsize);
int coh_write_int_to_file(char *path, int value);
list_t read_file_and_split(char * path, char *delimiter, int buffsize);

#endif
