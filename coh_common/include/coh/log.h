//
//  log.h
//
//  Author:
//       Jonathan Jason Dennis <theonejohnnyd@gmail.com>
//
//  Copyright (c) 2017 Meticulus Development
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LOG_H
#define LOG_H
#include <stdarg.h>
#include <stdio.h>

#ifndef LOG_TAG
#define LOG_TAG NULL
#endif

#ifdef __ANDROID__
#include <log/log.h>
#include <cutils/klog.h>
#define LOGE ALOGE
#define LOGK(...) klog_write(0, __VA_ARGS__) 
#else
#define LOGE(fmt, ...) fprintf(stderr,"E::%s::",LOG_TAG) ; fprintf(stderr, fmt,__VA_ARGS__); fprintf(stderr, "\n")
#endif //__ANDROID__

#ifdef DEBUG
#ifdef __ANDROID__
#define LOGD ALOGD
#define LOGW ALOGW
#define LOGI ALOGI
#else
#define LOGD(fmt, ...) fprintf(stderr,"D::%s::",LOG_TAG) ; fprintf(stderr, fmt,__VA_ARGS__); fprintf(stderr, "\n")
#define LOGW(fmt, ...) fprintf(stderr,"W::%s::",LOG_TAG) ; fprintf(stderr, fmt,__VA_ARGS__); fprintf(stderr, "\n")
#define LOGI(fmt, ...) fprintf(stderr,"I::%s::",LOG_TAG) ; fprintf(stderr, fmt,__VA_ARGS__); fprintf(stderr, "\n")
#endif //__ANDROID__
#else
#define LOGD(fmt, ...) while(0)
#define LOGW(fmt, ...) while(0)
#define LOGI(fmt, ...) while(0)
#endif //DEBUG

#endif //LOG_H
