/*
 * Code Of Honor
 * Copyright (c) 2018 Jonathan Dennis [Meticulus]
 *                               theonejohnnyd@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define LOG_TAG "coh_file"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <coh/file.h>
#include <stdlib.h>
#include <coh/log.h>

int coh_read_from_file(char *path, void* buff, int buffsize) {
    int fd = -1;
    int bytesread = 0;
    fd = open(path, O_RDONLY);
    if(fd <= 0) {
    	LOGE("Could not open file %s\n",path);
	    return 0;
	}

    bytesread = read(fd, buff, buffsize);
    close(fd);
    return bytesread;
}

int coh_write_to_file(char *path, void* buff, int buffsize) {
    int fd = -1;
    int byteswritten = 0;
    fd = open(path,  O_WRONLY | O_CREAT, 0666);
    if(fd <= 0) {
        LOGE("Could not open file %s\n",path);
	    return 0;
	}

    byteswritten = write(fd, buff, buffsize);
    close(fd);
    return byteswritten;
}

int coh_write_int_to_file(char *path, int value) {
	char buff[11] = {0}; //2147483647 max for int32
	sprintf(buff, "%d",value);
	return coh_write_to_file(path, buff, 11);
}

list_t read_file_and_split(char *path, char *delimiter, int buffsize) {
	list_t list = list_create(NULL, 0,1);
	char *buff = calloc(sizeof(char),buffsize);
	if(coh_read_from_file(path, buff, buffsize)) {
		buff[strlen(buff) -1] = '\0';
		char *entry = strtok(buff, delimiter);
			while(entry) {
				list_add_entry(&list, strdup(entry));
				entry = strtok(NULL, delimiter);
			}
	}
	free(buff);
	return list;
}

