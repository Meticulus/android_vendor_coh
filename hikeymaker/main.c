﻿//
//   Code Of Honor
//   Copyright (c) 2018 Jonathan Dennis [Meticulus]
//                                  theonejohnnyd@gmail.com
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
#define LOG_TAG "coh_hikeymaker"
#define DEBUG
#include <stdio.h>
#include <coh_hisi/teekeys.h>
#include <coh/log.h>
#include <coh/file.h>
#include <stdlib.h>
#include <cutils/properties.h>

int main (int argc, char *argv[])
{
	teekeys_t *keys = readteekeys("/hwvendor/etc/native_packages.xml");
	if(!keys) {
		LOGE("%s","Could not read keys!");
		return -1;
	}
	if(coh_write_to_file("/sys/module/tc_client_driver/parameters/fpkey", keys->fingerprintd.key, 32) != 32)
		LOGE("Error writing fpkey!");
	if(coh_write_to_file("/sys/module/tc_client_driver/parameters/kskey", keys->keystore.key, 32) != 32)
		LOGE("Error writing kskey!");
	if( coh_write_to_file("/sys/module/tc_client_driver/parameters/gkey", keys->gatekeeperd.key, 32) != 32)
		LOGE("Error writing gkey!");
	
	/* We need teecd to wait till we get these keys in place first so now teecd starts when we set this property */
	property_set("sys.starttee", "1");
	freeteekeys(keys);
	return 0;
}

